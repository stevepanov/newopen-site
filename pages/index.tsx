import Layout from "components/Layout";
import { Title } from "styles/indexStyles";

const IndexPage = () => {
  return (
    <Layout>
      <Title>Title</Title>
    </Layout>
  );
};

export default IndexPage;
