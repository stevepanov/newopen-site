import React from "react";
import App from "next/app";
import Head from "next/head";
import GlobalStyles from "styles/globalStyles";

class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props;
    return (
      <>
        <Head>
          {/* <link
            href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600&display=auto&subset=cyrillic"
            rel="stylesheet"
          /> */}
          {/* <link rel="icon" sizes="any" href="../static/favicon.svg" /> */}
        </Head>
        <GlobalStyles />
        <Component {...pageProps} />
      </>
    );
  }
}

export default MyApp;
