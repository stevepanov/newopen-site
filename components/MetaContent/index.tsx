import Head from "next/head";

const MetaContent = ({ meta: { title, description, image } }) => {
  const metaTitle = title || "";
  const metaDescription = description || "";
  const metaImage = image || "";

  return (
    <Head>
      <title>{metaTitle}</title>
      <meta property="og:title" content={metaTitle} />
      <meta name="description" content={metaDescription} />
      <meta name="og:description" content={metaDescription} />
      <meta property="og:type" content="website" />
      <meta property="og:image" content={metaImage} />
      <meta property="og:image:secure_url" content={metaImage} />
      <meta property="og:locale" content="ru_RU" />
      {/* <meta property="og:url" content="" /> */}
      {/* <meta property="og:site_name" content="" /> */}
      <meta property="og:image:width" content="1620" />
      <meta property="og:image:height" content="1527" />
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:title" content={metaTitle} />
      <meta name="twitter:description" content={description} />
      <meta name="twitter:image" content={metaImage} />
    </Head>
  );
};

export default MetaContent;
