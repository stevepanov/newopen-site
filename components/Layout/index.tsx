import React from "react";

import Header from "components/Header";
import Footer from "components/Footer";
import MetaContent from "components/MetaContent";

const Layout = ({ children, meta = {} }) => (
  <>
    <MetaContent meta={meta} />
    <Header />
    {children}
    <Footer />
  </>
);

export default Layout;
