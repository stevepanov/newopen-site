const withSourceMaps = require("@zeit/next-source-maps")();

const dev = process.env.NODE_ENV !== "production";

module.exports = withSourceMaps({
  crossOrigin: "anonymous",
  serverRuntimeConfig: {
    // serverAddress: dev ? '' : ''
  },
  publicRuntimeConfig: {
    // staticImagesServer: ``,
    // secret: 'aNDYcoTOLZKjukb-iOag9Jm3DZ9uqUS0erYUqxfif80tp_UAzsFbgEK05AgLl91x',
    // env: dev ? 'development' : 'production'
  },
});
