## Запуск в dev режиме

```
cd newopen-site
yarn install
yarn dev
```

## Запуск в production режиме

```
cd newopen-site
yarn install
yarn build
yarn start
```
